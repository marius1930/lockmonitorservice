## Lock Monitor Service

Purpose:  
Logs all login/lock/unlock on a PC.

**How to run it:**
- Compile it
- set DOTNET4=%SystemRoot%\Microsoft.NET\Framework64\v4.0.30319\
- set PATH=%PATH%;%DOTNET4%
- InstallUtil /i LockMonitorService.exe

**Obs:**
Logs to f:\LockMonitor.txt, this is hardcoded in LoggerSetup.cs  
Should be moved into a registry entry.