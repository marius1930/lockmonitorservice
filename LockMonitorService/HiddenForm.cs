﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using log4net;
using Microsoft.Win32;

namespace LockMonitorService {
    public partial class HiddenForm : Form {
        public HiddenForm() {
            InitializeComponent();
        }

        private void HiddenForm_Load(object sender, EventArgs e) {
            SystemEvents.SessionSwitch += SystemEvents_SessionSwitch;
            this.FormClosing += (o, args) => { SystemEvents.SessionSwitch -= SystemEvents_SessionSwitch; };
        }

        void SystemEvents_SessionSwitch(object sender, SessionSwitchEventArgs e) {
            EventLog.WriteEntry("LockMonitorService.Event", $"SessionLock: {e.Reason}");
            if (e.Reason == SessionSwitchReason.SessionLock) {
                //I left my desk
                LogManager.GetLogger("Service").Info("Locked");
                Console.WriteLine("I left my desk");
                File.WriteAllText("f:\\testy-test-test.txt", "hallo");
            }
            else if (e.Reason == SessionSwitchReason.SessionUnlock) {
                //I returned to my desk
                LogManager.GetLogger("Service").Info("Unlocked");
                Console.WriteLine("I returned to my desk");
            }
        }
    }
}
