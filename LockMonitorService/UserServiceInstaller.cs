﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace LockMonitorService {
    [RunInstaller(true)]
    public class UserServiceInstaller : Installer {
        public UserServiceInstaller() {
            ServiceProcessInstaller serviceProcessInstaller =
                new ServiceProcessInstaller();
            ServiceInstaller serviceInstaller = new ServiceInstaller();

            //# Service Account Information
            serviceProcessInstaller.Account = ServiceAccount.LocalSystem;
            serviceProcessInstaller.Username = null;
            serviceProcessInstaller.Password = null;

            //# Service Information
            serviceInstaller.DisplayName = "LockMonitor";
            serviceInstaller.StartType = ServiceStartMode.Automatic;

            //# This must be identical to the WindowsService.ServiceBase name
            //# set in the constructor of WindowsService.cs
            serviceInstaller.ServiceName = "LockMonitor";

            this.Installers.Add(serviceProcessInstaller);
            this.Installers.Add(serviceInstaller);
        }
    }
}
