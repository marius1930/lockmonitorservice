﻿using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using log4net;
using System.Windows.Forms;

namespace LockMonitorService {
    public class Service : ServiceBase {
        /// <summary>
        /// Public Constructor for WindowsService.
        /// - Put all of your Initialization code here.
        /// </summary>
        public Service() {
            this.ServiceName = "LockMonitor";
            this.EventLog.Log = "Application";

            // These Flags set whether or not to handle that specific
            //  type of event. Set to true if you need it, false otherwise.
            this.CanHandlePowerEvent = true;
            this.CanHandleSessionChangeEvent = true;    
            this.CanPauseAndContinue = true;
            this.CanShutdown = true;
            this.CanStop = true;
        }

        /// <summary>
        /// The Main Thread: This is where your Service is Run.
        /// </summary>
        static void Main() {
            LoggerSetup.Setup();
            LogManager.GetLogger("Service").Info("Initializing");

            ServiceBase.Run(new Service());
        }


        void RunMessagePump() {
            EventLog.WriteEntry("LockMonitorService.MessagePump", "Starting Message Pump");
            Application.Run(new HiddenForm());
        }

        /// <summary>
        /// Dispose of objects that need it here.
        /// </summary>
        /// <param name="disposing">Whether
        ///    or not disposing is going on.</param>
        protected override void Dispose(bool disposing) {
            base.Dispose(disposing);
        }

        /// <summary>
        /// OnStart(): Put startup code here
        ///  - Start threads, get inital data, etc.
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args) {
            base.OnStart(args);
            new Thread(RunMessagePump).Start();
            LogManager.GetLogger("Service").Info("- Service Start Event -");
        }

        /// <summary>
        /// OnStop(): Put your stop code here
        /// - Stop threads, set final data, etc.
        /// </summary>
        protected override void OnStop() {
            base.OnStop();
            LogManager.GetLogger("Service").Info("Service Stop Event");
            Application.Exit();
        }
        // https://msdn.microsoft.com/en-us/library/microsoft.win32.systemevents.aspx


        /// <summary>
        /// OnShutdown(): Called when the System is shutting down
        /// - Put code here when you need special handling
        ///   of code that deals with a system shutdown, such
        ///   as saving special data before shutdown.
        /// </summary>
        protected override void OnShutdown() {
            LogManager.GetLogger("Service").Info("Shutdown Event");
            base.OnShutdown();
        }

        /// <summary>
        /// OnPowerEvent(): Useful for detecting power status changes,
        ///   such as going into Suspend mode or Low Battery for laptops.
        /// </summary>
        /// <param name="powerStatus">The Power Broadcast Status
        /// (BatteryLow, Suspend, etc.)</param>
        protected override bool OnPowerEvent(PowerBroadcastStatus powerStatus) {
            LogManager.GetLogger("Service").Info($"Power Event: {powerStatus.ToString()}");
            return base.OnPowerEvent(powerStatus);
        }

        /// <summary>
        /// OnSessionChange(): To handle a change event
        ///   from a Terminal Server session.
        ///   Useful if you need to determine
        ///   when a user logs in remotely or logs off,
        ///   or when someone logs into the console.
        /// </summary>
        /// <param name="changeDescription">The Session Change
        /// Event that occured.</param>
        protected override void OnSessionChange(SessionChangeDescription changeDescription) {
            base.OnSessionChange(changeDescription);
            LogManager.GetLogger("Service").Info($"Trigger: {changeDescription.Reason.ToString()}");
            EventLog.WriteEntry("LockMonitorService.Event", $"Trigger: {changeDescription.Reason}");
        }
    }
}

