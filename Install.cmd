@ECHO OFF

REM The following directory is for .NET 4.x
set DOTNETFX2=%SystemRoot%\Microsoft.NET\Framework64\v4.0.30319\
set PATH=%PATH%;%DOTNETFX2%

echo Installing LockMonitorService...
echo ---------------------------------------------------
InstallUtil /i LockMonitorService.exe
echo ---------------------------------------------------
echo Done.